use anyhow::Context;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(deny_unknown_fields)]
pub struct Person {
    pub username: String,
    pub last_name: Option<String>,
    pub first_name: Option<String>,
    pub email: Option<String>,
}

#[derive(Clone, Debug)]
pub struct CourseSpec {
    pub id: String,
    pub students: Vec<Person>,
    pub instructors: Vec<Person>,
}

#[derive(Clone, Debug)]
pub struct CourseDelta<'a> {
    pub course: &'a CourseSpec,
}

#[derive(serde::Deserialize, Clone, Default, Debug)]
#[serde(deny_unknown_fields)]
pub struct Courses {
    pub courses: Vec<String>,
}

#[derive(serde::Deserialize, serde::Serialize, Clone, Default, Debug)]
#[serde(deny_unknown_fields)]
pub struct Instructors {
    pub instructors: Vec<Person>,
}

#[derive(serde::Deserialize, serde::Serialize, Clone, Default, Debug)]
#[serde(deny_unknown_fields)]
pub struct Students {
    pub students: Vec<Person>,
}

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct JhReply<T> {
    #[serde(default, flatten)]
    inner: Option<T>,
    #[serde(default)]
    message: Option<String>,
    success: bool,
}

macro_rules! impl_methods {
    ($($method:ident -> $verb:ident($($param:ident),*),)*) => {
        $(impl_methods!(IMPL, $method, $verb( $($param),*) ) ;)*
    };
    (IMPL, $method:ident, $verb:ident ()) => {
        pub fn $method<Path: AsRef<str>>(&self, path: Path) -> reqwest::RequestBuilder {
            self.request_builder(reqwest::Method::$verb, path)
        }
    };
    (IMPL, $method:ident, $verb:ident (data)) => {
        pub fn $method<Path: AsRef<str>, D>(&self, path: Path, data: D) -> reqwest::RequestBuilder
            where
                  D: serde::Serialize,
        {
            self.request_builder(reqwest::Method::$verb, path)
                .form(&data)
        }
    };
}

macro_rules! impl_api {
    (IMPL $verb:ident ($($post_param:ident: $ser:ty),*) $path:tt : $method:ident ( $($param:ident: $get_param_type:ty),* ) -> $deser:ty) => {
        pub async fn $method(&self
            $(, $param : $get_param_type)*
            $(, $post_param : $ser)*
        ) -> Result<$deser, anyhow::Error> {
            let url = format!($path, $($param = $param),*);
            let request = self.$verb(&url $(, $post_param)*);
            let reply = request.send().await.context("sending ngshare API request")?;

            use reqwest::StatusCode;
            if ! reply.status().is_success() {
                match reply.status() {
                    StatusCode::OK => (),
                    StatusCode::FORBIDDEN => anyhow::bail!("Forbidden"),
                    _ => anyhow::bail!("unimplemented StatusCode {:?} requesting {} {}", reply.status(), stringify!($method), url),
                }
            }
            log::debug!("Reply: {:?}", reply);
            let reply: JhReply<_> = reply.json().await.context("decoding ngshare API response as JSON")?;

            log::debug!("{:?}", reply);

            if reply.success {
                Ok(reply.inner.unwrap())
            } else {
                anyhow::bail!("Request {} {} failed with {}", stringify!($method), url, reply.message.unwrap());
            }
        }
    };
    ($($verb:ident ($($post_param:ident: $ser:ty),*) $path:tt : $method:ident ( $($param:ident: $get_param_type:ty),* ) -> $deser:ty,)*) => {
        $(impl_api!(IMPL $verb ($($post_param : $ser),*) $path : $method ($( $param : $get_param_type ),*) -> $deser);)*
    };
}

pub struct NgShareApi {
    pub base_url: String,
    pub token: String,
}

impl NgShareApi {
    pub fn from_env() -> Result<Self, anyhow::Error> {
        dotenv::dotenv().ok();
        Ok(Self {
            base_url: std::env::var("JUPYTERHUB_API_URL")?,
            token: std::env::var("JUPYTERHUB_API_TOKEN")?,
        })
    }

    pub fn new(base_url: String, token: String) -> Self {
        Self { base_url, token }
    }

    fn request_builder(
        &self,
        method: reqwest::Method,
        path: impl AsRef<str>,
    ) -> reqwest::RequestBuilder {
        let url = format!("{}/services/ngshare/{}", self.base_url, path.as_ref());
        log::trace!("Request builder for {}", url);
        reqwest::Client::new().request(method, &url).header(
            reqwest::header::AUTHORIZATION,
            &format!("token {}", self.token),
        )
    }

    impl_methods! {
        get -> GET(),
        // https://github.com/seanmonstar/reqwest/issues/274
        post_no_data -> POST(),
        post -> POST(data),
        put -> PUT(data),
        delete -> DELETE(),
    }

    impl_api! {
        post_no_data() "course/{course_id}": create_course(course_id: &str) -> (),
        get() "courses": courses() -> Courses,

        get() "students/{course_id}": students(course_id: &str) -> Students,
        post(student: Person) "student/{course_id}/{student_id}": add_student_to_course(course_id: &str, student_id: &str) -> (),
        delete() "student/{course_id}/{student_id}": delete_course_student(course_id: &str, student_id: &str) -> (),

        get() "instructors/{course_id}": instructors(course_id: &str) -> Instructors,
        post(instructor: Person) "instructor/{course_id}/{instructor_id}": add_instructor_to_course(course_id: &str, instructor_id: &str) -> (),
        delete() "instructor/{course_id}/{instructor_id}": delete_course_instructor(course_id: &str, instructor_id: &str) -> (),

        delete() "course/{course_id}": delete_course(course_id: &str) -> (),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn list_courses() -> Result<(), anyhow::Error> {
        env_logger::try_init().ok();
        let api = NgShareApi::from_env()?;
        api.courses().await.expect("fetch courses");
        Ok(())
    }

    #[tokio::test]
    async fn create_and_delete() -> Result<(), anyhow::Error> {
        env_logger::try_init().ok();
        let api = NgShareApi::from_env()?;
        let name = "this-course-is-for-a-test-and-should-be-deleted";

        api.create_course(name).await?;
        assert!(api.create_course(name,).await.is_err());

        api.delete_course(name).await?;
        assert!(api.delete_course(name).await.is_err());
        Ok(())
    }
}
