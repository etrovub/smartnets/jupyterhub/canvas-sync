use std::collections::HashMap;

use anyhow::Context;
use canvas_sync::ngshare::Person;
use canvasapi::prelude::*;

// KEEP THIS IN SYNC WITH JUPYTERHUB_CONFIG
fn name_map(name: impl AsRef<str>) -> String {
    let name = name.as_ref();
    let name = if name.len() > 40 {
        let name = name.split('@').next().unwrap();
        name.replace('.', "-")
    } else {
        name.to_string()
    };
    if name.len() > 40 {
        name[..40].to_string()
    } else {
        name
    }
}

fn user_filter(kind: &str, user: User) -> Option<(String, Person)> {
    log::debug!("Looking at {} {:?}", kind, user);
    if user.login_id.is_none() && user.email.is_none() {
        log::warn!(
            "{:?} does not have a login id or email. Skipping",
            user.sortable_name
        );
    }
    if user.sortable_name.is_none() {
        log::warn!("{:?} does not have a name. Skipping", user);
    }
    let sortable = user.sortable_name.clone()?;
    let mut sortable = sortable.split(',');
    let last_name = sortable.next().unwrap().trim();
    let first_name = sortable.next().unwrap().trim();
    let username = name_map(
        user.login_id
            .as_ref()
            .or(user.email.as_ref())?
            .to_lowercase(),
    );

    log::info!("user {}, {} will be {}", last_name, first_name, kind);
    Some((
        username.clone(),
        Person {
            username,
            last_name: Some(last_name.into()),
            first_name: Some(first_name.into()),
            email: Some(user.email.unwrap()),
        },
    ))
}

struct Metrics {
    courses: Vec<CourseMetrics>,
    jupyter_host: String,
}

struct CourseMetrics {
    ngshare_course_id: String,
    canvas_course_id: u64,
    canvas_host: String,

    status: u32,

    total_students: u64,
    total_instructors: u64,
    added_students: u64,
    added_instructors: u64,
    removed_students: u64,
    removed_instructors: u64,
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    env_logger::init();

    let config = config::Config::builder()
        .add_source(config::File::with_name("canvas-sync.toml"))
        .build()?;

    let jupyter_host = config.get_string("jupyter.host")?;
    let jupyter_api_token = config.get_string("jupyter.token")?;
    log::info!("Jupyter host {}", jupyter_host);

    let ngshare = canvas_sync::ngshare::NgShareApi::new(jupyter_host.clone(), jupyter_api_token);

    let ngshare_courses = ngshare.courses().await?.courses;

    for course in &ngshare_courses {
        log::info!("ngshare course {}", course);
    }

    let courses = config.get_table("canvas.courses")?;

    let mut errors = false;

    let mut metrics = Metrics {
        courses: vec![],
        jupyter_host: jupyter_host.clone(),
    };

    for (ngshare_id, spec) in courses {
        let anyhow_ngshare_context = || format!("for ngshare course {}", ngshare_id);
        let course_result: anyhow::Result<()> = async {
            let spec = spec.into_table().context("parsing canvas-sync.toml spec")?;
            let canvas_id = spec.get("canvas_id").unwrap().clone().into_uint()?;

            let canvas_host: String = spec.get("host").unwrap().clone().into_string()?;
            let canvas_token: String = spec.get("token").unwrap().clone().into_string()?;
            log::info!("Canvas host {}", canvas_host);

            metrics.courses.push(CourseMetrics {
                ngshare_course_id: ngshare_id.clone(),
                canvas_course_id: canvas_id,
                canvas_host: canvas_host.clone(),
                status: 0,
                total_students: 0,
                total_instructors: 0,
                added_students: 0,
                added_instructors: 0,
                removed_students: 0,
                removed_instructors: 0,
            });

            let metric = metrics.courses.last_mut().unwrap();

            let anyhow_course_context = || format!("for course {} on {}", canvas_id, canvas_host);

            let canvas = CanvasInformation::new(&canvas_host, &canvas_token);

            let course = Canvas::get_course(canvas_id as _)?
                .fetch(&canvas)
                .await
                .with_context(anyhow_course_context)?
                .inner();
            log::info!("course {}: {:?}", canvas_id, course.name);

            let students = course
                .get_users()?
                .add_parameter(EnrollmentType::Student)
                .fetch(&canvas)
                .await
                .with_context(anyhow_course_context)?
                .inner();

            let mut new_ngshare_students: HashMap<_, _> = students
                .into_iter()
                .filter_map(|u| user_filter("student", u))
                .collect();

            let teachers = course
                .get_users()?
                .add_parameter(EnrollmentType::Teacher)
                .fetch(&canvas)
                .await
                .with_context(anyhow_course_context)?
                .inner();
            let ta_s = course
                .get_users()?
                .add_parameter(EnrollmentType::TA)
                .fetch(&canvas)
                .await
                .with_context(anyhow_course_context)?
                .inner();

            let mut new_ngshare_instructors: HashMap<_, _> = teachers
                .into_iter()
                .chain(ta_s.into_iter())
                .filter_map(|u| user_filter("instructor", u))
                .collect();
            metric.total_instructors = new_ngshare_instructors.len() as u64;

            // Only keep students who are not teachers.
            new_ngshare_students.retain(|key, _v| !new_ngshare_instructors.contains_key(key));
            metric.total_students = new_ngshare_students.len() as u64;

            if ngshare_courses.contains(&ngshare_id) {
                log::info!(
                    "Found matching course for (canvas) {:?}: (ngshare) {}",
                    course.name,
                    ngshare_id
                );

                let old_students = ngshare
                    .students(&ngshare_id)
                    .await
                    .with_context(anyhow_course_context)?;
                let old_instructors = ngshare
                    .instructors(&ngshare_id)
                    .await
                    .with_context(anyhow_course_context)?;
                log::info!(
                    "students: {:?}, instructors: {:?}",
                    old_students,
                    old_instructors
                );

                log::info!(
                    "Should become {:?} and {:?}",
                    new_ngshare_students,
                    new_ngshare_instructors
                );

                // Delete users that shouldn't be there;
                // Update names and emails
                for old_student in &old_students.students {
                    if let Some(_old_student) = new_ngshare_students.remove(&old_student.username) {
                        // Student exists on canvas and ngshare, update details
                        // TODO
                    } else {
                        // Student does exist on ngshare, but not on canvas, remove
                        log::warn!(
                            "Deleting student {} from {}",
                            old_student.username,
                            ngshare_id
                        );
                        ngshare
                            .delete_course_student(&ngshare_id, &old_student.username)
                            .await?;
                        metric.removed_students += 1;
                    }
                }

                for old_instructor in &old_instructors.instructors {
                    if let Some(_old_instructor) =
                        new_ngshare_instructors.remove(&old_instructor.username)
                    {
                        // instructor exists on canvas and ngshare, update details
                        // TODO
                    } else {
                        // instructor does exist on ngshare, but not on canvas, remove
                        log::warn!(
                            "Deleting instructor {} from {}",
                            old_instructor.username,
                            ngshare_id
                        );
                        ngshare
                            .delete_course_instructor(&ngshare_id, &old_instructor.username)
                            .await
                            .with_context(|| {
                                format!(
                                    "deleting instructor {} to {}",
                                    old_instructor.username, ngshare_id
                                )
                            })?;
                        metric.removed_instructors += 1;
                    }
                }
            } else {
                log::warn!(
                    "Could not find matching course on ngshare for {:?}. Creating!",
                    course.name
                );

                ngshare.create_course(&ngshare_id).await?;
                // TODO: add email and name to instructor
            }

            // Add students
            for student in new_ngshare_students.values() {
                ngshare
                    .add_student_to_course(&ngshare_id, &student.username, student.clone())
                    .await
                    .with_context(|| {
                        format!("adding student {} to {}", student.username, ngshare_id)
                    })?;
                metric.added_students += 1;
            }

            // Add instructors
            for instructor in new_ngshare_instructors.values() {
                ngshare
                    .add_instructor_to_course(&ngshare_id, &instructor.username, instructor.clone())
                    .await
                    .with_context(|| {
                        format!(
                            "adding instructor {} to {}",
                            instructor.username, ngshare_id
                        )
                    })?;
                metric.added_instructors += 1;
            }

            metric.status = 1;
            Result::<_, anyhow::Error>::Ok(())
        }
        .await
        .with_context(anyhow_ngshare_context);
        match course_result {
            Ok(()) => {
                log::info!("Course {} finished.", ngshare_id);
            }
            Err(e) => {
                log::error!("Course {} threw an error: {:?}.", ngshare_id, e);
                errors = true;
            }
        }
    }

    // Write metrics to file
    if let Ok(metrics_path) = config.get_string("metrics.path") {
        let mut f = std::fs::File::create(metrics_path)?;
        metrics.write_to(&mut f)?;
    }

    if errors {
        anyhow::bail!("Finished with errors");
    } else {
        Ok(())
    }
}

impl Metrics {
    pub fn write_to<W: std::io::Write>(&self, w: &mut W) -> std::io::Result<()> {
        let jupyter_host = &self.jupyter_host;

        // Status
        writeln!(w, "# HELP canvas_sync_status Canvas sync status")?;
        writeln!(w, "# TYPE canvas_sync_status untyped")?;
        let status = if self.courses.iter().any(|c| c.status == 0) {
            0.0
        } else {
            1.0
        };
        writeln!(
            w,
            "canvas_sync_status{{service=\"jupyter::canvas-sync\", jupyter_host=\"{jupyter_host}\"}} {status}"
        )?;

        // Number of courses
        writeln!(
            w,
            "# HELP canvas_sync_total_courses Total number of courses"
        )?;
        writeln!(w, "# TYPE canvas_sync_total_courses gauge")?;
        writeln!(
            w,
            "canvas_sync_total_courses{{service=\"jupyter::canvas-sync\", jupyter_host=\"{jupyter_host}\"}} {}",
            self.courses.len()
        )?;

        // Course statuses
        self.write_metric(
            w,
            "canvas_sync_course_status",
            "Course synchronisation status",
            "gauge",
            |c| c.status as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_total_students",
            "Total number of students in course",
            "gauge",
            |c| c.total_students as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_total_instructors",
            "Total number of instructors in course",
            "gauge",
            |c| c.total_instructors as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_added_students",
            "Number of students added to course",
            "gauge",
            |c| c.added_students as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_added_instructors",
            "Number of instructors added to course",
            "gauge",
            |c| c.added_instructors as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_removed_students",
            "Number of students removed from course",
            "gauge",
            |c| c.removed_students as f64,
        )?;
        self.write_metric(
            w,
            "canvas_sync_removed_instructors",
            "Number of instructors removed from course",
            "gauge",
            |c| c.removed_instructors as f64,
        )?;

        Ok(())
    }

    fn write_metric<W: std::io::Write>(
        &self,
        w: &mut W,
        label: &'static str,
        help: &'static str,
        ty: &'static str,
        getter: impl Fn(&CourseMetrics) -> f64,
    ) -> std::io::Result<()> {
        let jupyter_host = &self.jupyter_host;

        writeln!(w, "# HELP {label} {help}",)?;
        writeln!(w, "# TYPE {label} {ty}")?;
        for course in &self.courses {
            let CourseMetrics {
                ngshare_course_id,
                canvas_course_id,
                canvas_host,
                status: _,
                total_students: _,
                total_instructors: _,
                added_students: _,
                added_instructors: _,
                removed_students: _,
                removed_instructors: _,
            } = course;
            let status = getter(course);
            writeln!(
                w,
                "{label}{{service=\"jupyter::canvas-sync\", jupyter_host=\"{jupyter_host}\", ngshare_course_id=\"{ngshare_course_id}\", canvas_course_id=\"{canvas_course_id}\", canvas_host=\"{canvas_host}\"}} {status}",
            )?;
        }

        Ok(())
    }
}
