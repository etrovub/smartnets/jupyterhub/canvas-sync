FROM rust:latest as build-image

WORKDIR /src

COPY . .

RUN cargo build --release


FROM debian:latest

RUN apt-get update && \
        apt-get install -y \
          openssl \
          ca-certificates \
        && \
        apt-get clean && rm -rf /var/lib/apt/lists/*

COPY --from=build-image /src/target/release/canvas-sync /usr/local/bin/canvas-sync
